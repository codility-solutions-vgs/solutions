package test;

import java.util.Arrays;

public class ArrayRotation {
	public static void main(String [] args) {
		// TODO Auto-generated method stub
		//62%
		
		int[] test = {3, 8, 9, 7, 6};
		int rotations = 5;
		
		int result [] = solution(test, rotations);
		
		System.out.println(Arrays.toString(result));

	}
	
	public static int[] solution(int [] A, int K) {
		
		int[] temp = new int[A.length];
		int indexMoved;
		
		if(K > A.length) {
			indexMoved = ((A.length-1) % K) - 1;
			indexMoved = indexMoved - 1;
		} else {
			indexMoved = K;
		}
		
		int top = 0;
		int original = 0;
		int start = indexMoved; 
		
		while(original < A.length) {
			
			if(start < A.length) {
				temp[start] = A[original];
			} else if(start >= A.length) {
				temp[top] = A[original];
				top++;
			}
			start++;
			original++;
		}
		
		return temp;
	}
}
