package test;
import java.util.ArrayList;
public class BinaryGap {
	
	public static void main(String [] args) {
		
		System.out.println(solution(32));
		System.out.println(solution(54));
		System.out.println(solution(678));
		System.out.println(solution(1987));
		System.out.println(solution(2239));
		System.out.println(solution(3579));
		System.out.println(solution(8888));
		
	}
	
	public static int solution(int N) {
		//100%
	     ArrayList<Integer> binaryStack = new ArrayList<>();
	     String binaryString = "";
	     int longestBinaryGap = 0, counter = 0;
	     char temp, temp2;
	     boolean firstOne = false;
	     int t = N;

//	     getting the reversed binary token
	     while(N > 0) {
	         binaryStack.add(N%2);
	         N = N/2;
	     }

//	     converting the binary token to string
	     for(int a = binaryStack.size()-1; a >= 0; a--) {
	    	 binaryString = binaryString + binaryStack.get(a);
	     }
	     
//	     trim string of the trailing zeroes until encoutered the first 1
//	     for(int a = 0; a < binaryString.length()-1; a++) {
//	    	 
//	     }

	     System.out.println(t +": "+ binaryString);
	     
	     for(int a = 0; a < binaryString.length()-1; a++) {
	         
	         temp = binaryString.charAt(a);
	         temp2 = binaryString.charAt(a+1);
//	         System.out.println("temp:" + temp);
	         if(temp == '0') {
	             counter++;
	             
	         } else {
	             counter = 0;
	         }

	         if(longestBinaryGap < counter && temp2 == '1') {
	             longestBinaryGap = counter;
	         }
	     }

	     return longestBinaryGap;
	 }
}


