package test;

public class CountFactors {
	
	public static void main(String [] args) {
		System.out.println(solution(13));
		System.out.println(solution(24));
		System.out.println(solution(15));
		System.out.println(solution(13245));
	}
	
    public static int solution(int N) {
        // write your code in Java SE 8
    	// 71%
        int numberOfFactors = 1;
        int lastNumber = N / 2;

        for(int a = 1; a <= lastNumber; a++) {
            if(N%a == 0) {
                numberOfFactors++;
            }
        }


        return numberOfFactors;
    }
}
