package test;

import java.util.Arrays;

public class FirstUnique {
	
	public static void main(String [] args) {
		int A [] = {4, 10, 5, 4, 2, 10};
		int B [] = {6,4,4,6};
		
		System.out.println(solution(B));
	}
	
	public static int solution(int[] A) {
		//54%
        boolean isUnique = true;
        int temp = -1;

        for(int a = 0; a < A.length; a++) {
            for(int b = 0; b < A.length; b++) {
            	
            	if(a==b) {
            		continue;
            	}
            	
                if(A[a] == A[b]) {
                	isUnique = false;
                	break;
                } else if(A[a] != A[b]) {
                	isUnique = true;
                }
            }
            if(isUnique) {
            	System.out.println("val: " + A[a] +"index: " + a);
                temp = A[a];
                break;
            }
        }
        
        if(!isUnique) {
        	return -1;
        }

        return temp;
    }
}
