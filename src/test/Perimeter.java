package test;

import java.util.ArrayList;

public class Perimeter {
	public static void main(String [] args) {
		System.out.println(solution(13));
		System.out.println(solution(24));
		System.out.println(solution(15));
		System.out.println(solution(13245));
	}
	
    public static int solution(int N) {
        // write your code in Java SE 8
        int numberOfFactors = 1;
        int lastNumber = N / 2;
//        int factors [] = new int[lastNumber];
        ArrayList<Integer> factors = new ArrayList<>();

        for(int a = 1; a <= lastNumber; a++) {
            if(N%a == 0) {
                numberOfFactors++;
                factors.add(a);
            }
        }


        return numberOfFactors;
    }
}
